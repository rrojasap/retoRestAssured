import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TasksTest.class,
        UsersTest.class
})
public class TestSuite {
}

